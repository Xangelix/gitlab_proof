# gitlab_proof

[Verifying my OpenPGP key: openpgp4fpr:9DB06DF7B4B18DF459A0343594475C8B94E4698D]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
